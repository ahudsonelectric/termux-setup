#!/data/data/com.termux/files/usr/bin/env bash

cd "${0%/*}" || exit 1

pkg upgrade

pkg install bash-completion curl git man ncurses-utils neofetch openssh vim

for file in {bash,input,vim}rc gitconfig; do
  src="${PWD}/.${file}"
  dest="${HOME}/.${file}"
  if [[ ! -L "${dest}" ]]; then
    printf "link user's dotfile: %s\n" "${file}"
    ln -sv "${src}" "${dest}"
  fi
done
